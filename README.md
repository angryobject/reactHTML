A tiny module to help insert html content into react components with ES2015 syntax.
Because typing `dangerouslySetInnerHTML={{__html: someHMTLText}}` is hard. But don't forget:

Improper use of the innerHTML can open you up to a cross-site scripting (XSS) attack.

Usage:

```javascript
import reactHTML from 'reactHTML';

//...

<div {...reactHTML(someHMTLText)} />
```
